using System;
using Xunit;
using CostumExceptions.Exceptions;
using CostumExceptions;

namespace MyCostumExceptionTest
{
    public class UnitTest1
    {
        [Fact]
        public void Check_If_Method_Throws_MyCostumException()
        {
            //Arrange
            //Act Assert
            Assert.Throws<MyCostumException>(() => Program.TestException(true));  

        }
    }
}
