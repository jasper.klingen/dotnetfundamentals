﻿using System;

namespace Drawsquare
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter the size of your square");
            string sizestr = Console.ReadLine();
            try
            {
                Console.WriteLine(constructSquare(int.Parse(sizestr)));
            }
            catch (Exception e)
            {
                Console.WriteLine("Must be a number" + e);
            }

        }

        private static string constructSquare(int size)
        {
            string res = "";
            if (size < 0) size *= -1;
            res += topandbottom(size);
            res += sides(size);
            res += topandbottom(size);
            return res;
        }

        private static string sides(int size)
        {
            string res = "";
            string spaces = addSpaces(size);
            for (int i = 0; i < size; i++)
            {
                res += "*" + spaces + "*";
                res += System.Environment.NewLine;
            }
            return res;
        }

        private static string addSpaces(int size)
        {
            string res = "";
            for (int i = 0; i < (size*1.8)-2; i++)
                res += " ";
            return res;
        }

        private static string topandbottom(int size)
        {
            string res = "";
            for (int i = 0; i < size*1.8; i++)
            {
                res += "*";
            }
            res += System.Environment.NewLine;
            return res;
        }
    }
}
