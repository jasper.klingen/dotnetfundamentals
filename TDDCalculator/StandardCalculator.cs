﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDDCalculator
{
    public class StandardCalculator
    {
        public int Add(int lhs, int rhs)
        {
            return lhs + rhs;
        }

        public double Divide(int lhs, int rhs)
        {
            return lhs / rhs;
        }
        public int Subtracks(int lhs, int rhs)
        {
            return lhs - rhs;
        }
    }
}
