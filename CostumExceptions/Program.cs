﻿using CostumExceptions.Exceptions;
using System;

namespace CostumExceptions
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            try
            {
                TestException(true);
            }
            catch(MyCostumException ex)
            {
                Console.WriteLine("This is my costum exception here" + ex.Message);
            }
            catch(Exception ex)
            {
                Console.WriteLine("This is a normal exception" + ex.Message);
            }
        }
        public static void TestException(bool throwException)
        {
            if (throwException)
                throw new MyCostumException();
        }
    }
}
