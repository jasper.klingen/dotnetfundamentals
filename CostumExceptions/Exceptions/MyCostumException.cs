﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostumExceptions.Exceptions
{
    public class MyCostumException : Exception
    {
        public MyCostumException()
        {
        }

        public MyCostumException(string message) : base(message)
        {
        }

    }
}
