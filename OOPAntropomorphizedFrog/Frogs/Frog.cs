﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPAntropomorphizedFrog.Frogs
{
    public abstract class Frog
    {
        public string Name { get; set; }
        public double Weight { get; set; }

        public Frog()
        {
            Name = "unnamed";
            Weight = 100;
            Croak();
            SayName();

        }

        public Frog(string name, double weight)
        {
            Name = name;
            Weight = weight;
            SayName();
        }

        public void Croak()
        {
            Console.WriteLine("Ribbit");
        }
        public void Hop(double distance)
        {
            Console.WriteLine($"The frog hopped {distance} cm");
        }
        public void SayName()
        {
            Console.WriteLine(Name);
        }
                    
    }
}
