﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPAntropomorphizedFrog.Frogs
{
    class ScientistFrog : Frog
    {
        public string Subjects { get; set; }

        public ScientistFrog(string name, double weight, string subjects) : base(name, weight)
        {
            Subjects = subjects;
        }
    }
}
