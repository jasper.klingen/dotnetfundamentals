using System;
using TDDCalculator;
using Xunit;

namespace TDDCalculatorTest
{
    public class TDDCalculatorTest
    {
        [Theory]
        [InlineData(1, 2, 3)]
        [InlineData(-1, -1, -2)]
        public void Add_TwoNumbers_ReturnSumOfNumbers(int lhs, int rhs, int expected)
        {
            //Arrange
            StandardCalculator calculator = new StandardCalculator();

            //Act
            int actual = calculator.Add(lhs, rhs);
            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Divide_TwoNumbers_ReturnDivisionOfNumbers()
        {
            //Arrange
            StandardCalculator calculator = new StandardCalculator();
            int lhs = 12;
            int rhs = 9;
            double expected = lhs / rhs;
            //Act
            double actual = calculator.Divide(lhs, rhs);
            //Assert
            Assert.Equal(expected, actual);


        }
        [Fact]
        public void Divide_WithZero_ShouldTrowDivideByZeroException()
        {
            //Arrange
            StandardCalculator calculator = new StandardCalculator();
            int lhs = 12;
            int rhs = 0;
            //Act Assert
            Assert.Throws<DivideByZeroException>(() => calculator.Divide(lhs, rhs));
        }


}
}
