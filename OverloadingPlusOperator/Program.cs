﻿using OverloadingPlusOperator.Helpers;
using System;

namespace OverloadingPlusOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Box box01cust01 = new Box
            {
                Weight = 100,
                Volume = 6,
                Value = 10000
            };
            Box box02cust02 = new Box
            {
                Weight = 100,
                Volume = 6,
                Value = 10000
            };
            if(box01cust01 == box02cust02)
                Console.WriteLine("Equal");

            Box totalBox = box01cust01 + box02cust02;
            CalculateInsurance(totalBox);
        }
        public static void CalculateInsurance(Box total)
        {
            double insuranceValue = total.Value * total.Volume / total.Weight;
            Console.WriteLine(insuranceValue);
        }
    }
}
