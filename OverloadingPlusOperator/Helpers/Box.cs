﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverloadingPlusOperator.Helpers
{
    class Box
    {
        public double Weight { get; set; }
        public double Value { get; set; }
        public double Volume { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Box box &&
                   Weight == box.Weight &&
                   Value == box.Value &&
                   Volume == box.Volume;
        }

        public static Box operator +(Box first, Box second)
        {
            return new Box
            {
                Value = first.Value + second.Value,
                Volume = first.Volume + second.Volume,
                Weight = first.Weight + second.Weight
            };
        }
        
    }
}
